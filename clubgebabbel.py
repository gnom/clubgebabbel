#!/usr/bin/python
# -*- coding: utf-8 -*-

import tweepy
import datetime
import pickle
import json
import sys
import os
import random

path = os.path.dirname(os.path.abspath(__file__)) + "/"

specialfile = path+'special.json'
regularfile = path+'regular.json'
regularday = [2, 4] # 1 = Mo, 2 = Di, 3 = Mi, 4 = Do, 5 = Fr, 6 = Sa, 7 = So
minsleep = 1
maxsleep = 120*60 #set to 0 to disable randam sleeptime

if "--debug" in sys.argv:
    maxsleep = 0

def getTweet():
    date = str(datetime.date.today())
    special = json.load(open(specialfile, 'r'))
    if date in special:
        print(special[date])
        return(special[date])
    else:
        if datetime.date.today().isoweekday() in regularday:
            regular = json.load(open(regularfile, 'r'))
            return(regular[random.randint(0, len(regular)-1)])
        else:
            print("Heute habe ich leider keinen Tweet für dich")
            sys.exit(0)

oafile = path+'clubgebabbel.oauth'

with open(oafile, "rb") as handle:
    oauth = pickle.loads(handle.read())

auth = tweepy.OAuthHandler(oauth['conskey'], oauth['conssec'])
auth.set_access_token(oauth['acctoken'], oauth['accsecret'])

try:
    twitter = tweepy.API(auth)
except tweepy.TweepError:
    print("Error! Something went terribly wrong!")
    print(TweepError)


try:
    tweet = getTweet()
    if minsleep and maxsleep and (minsleep < maxsleep):
        from time import sleep
        stime = random.randint(minsleep, maxsleep)
        print("waiting for %ds" % stime)
        sleep(stime)
    if "--debug" in sys.argv:
        print(str(datetime.datetime.now()) + " " + tweet)
    else:
        twitter.update_status(tweet)
except tweepy.TweepError as e:
    print("Error! Something went terribly wrong!")
    print(e)

