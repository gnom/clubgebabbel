# clubgebabbel

Der Twitterbot für den Club!

Der Bot twittert aus einer Liste von regelmäßigen Ankündigungen, bei jeden Aufruf der an einem (oder mehreren) konfigurierten Wochentag(en) geschieht, eine zufällig ausgewählte Ankündigung.

Zusätlich besteht die Möglichkeit, zu einem bestimmten Datum, eine spezielle Nachricht zu vertwittern. Trifft diese mit einer regelmäßigen Ankündigung zusammen wird nur die *Spezialankündigung* getwittert und kein regelmäßiger Tweet.

## Installation

1. `git clone https://git.gnom.be/telegnom/clubgebabbel.git`
1. `./oauth_is_a_dancer.py` ausführen und den Anweisungen folgen
1. Die Texte für die regelmäßigen Tweets (*regular.json*) anpassen und ggf. Tweets für "Special Events" in die *special.json* eintragen.
1. Das Script einmal am Tag per Cron aufrufen lassen `1 10 * * * /full/path/to/clubgebabbel.py > /dev/null 2>&1`
